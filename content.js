//CHANGE THIS TO YOUR PERSONAL TOKEN
//You can find your personal token by browsing to a detail page of a product on colruyt.be (after logging in).
//Check Network > XHR in Chrome Dev Tools and look for a #####.json (eg. 20755.json), you can find you token under Authorization within the Request headers.
//Note: changes on every log-in/log-out.

// > PSEUDOCODE
//
// GetTokenFromStorage
//   if Token != undefined:
//     set global AUTH_TOKEN
//     checkToken
//       success > doAllThethings
//       error > getNewToken
//   else:
//     getNewToken
//       success > doAllThethings
//       error > getNewToken


var AUTH_TOKEN = ''

// chrome.storage.sync.clear();

// Save it using the Chrome extension storage API.
function saveToStorage(token) {
  chrome.storage.sync.set({'token': token}, function() {
    console.log('Settings saved');
  });
}

// Read it using the storage API
function getFromStorage(callback) {
  chrome.storage.sync.get('token', callback);
}

// Check one product to see if our key is still valid
function checkToken(token) {
        console.log("checkToken");
        $.ajax({
             url: 'https://eproductmw.colruytgroup.com/eproductmw/rest/v1/nl/stores/3515/products.json?id=5331',
             headers: { 'Authorization': AUTH_TOKEN},
             type: 'GET',
             success: function(res) {
               console.log("GREAT SUCCESS! Let's continue with this token");
               console.log(res);
               main();
             },
             error: function(res) {
               console.log("ERROR! Seems like we need to get a new token");
               getNewToken(handleNewToken);
               console.log(res);
             }
           });
       }

function getNewToken(callback) {
  $.ajax({
      url: 'https://www.colruyt.be/nl',
      type: 'GET',
      success: callback
    });
}

function handleNewToken(res) {
  var regex_token = /xtra-token=\"(.*)\"\s/g;
  auth_token = "Bearer " + rgx_data(res, regex_token)[0];
  console.log(auth_token);
  AUTH_TOKEN = auth_token;

  saveToStorage(auth_token);
  checkToken(AUTH_TOKEN)
}

function rgx_data(i, r) {
  var matches, output = [];
  while (matches = r.exec(i)) {
      output.push(matches[1]);
  }

  return output
}

getFromStorage(function(items) {
  console.log("GetFromStorage");
  console.log("Token fetched");
  console.log(items.token);

  if (items.token != undefined) {
    AUTH_TOKEN = items.token
    checkToken(AUTH_TOKEN);
  } else {
    getNewToken(handleNewToken);
  }
});

function main() {
  /* Discover type of page */
  // topBranchPage
  // branchPage
  // detailPage
  // checkout

  pageType = ''

  if ($(document.body).hasClass('topBranchPage') || $(document.body).hasClass('filterTopBranchPage') || $(document.body).hasClass('branchPage') || $(document.body).hasClass('lastReservationPage')) {
    console.log("branch page");
    pageType = 'branch';
  } else if ($(document.body).hasClass('detailPage')) {
    console.log("detail page");
    pageType = 'detail';
  } else if ($(document.body).hasClass('checkout')) {
    console.log("checkout page");
    pageType = 'checkout';
  }

  //REGEX SEARCH TO POPULATE PRODUCT ID LIST
  var regex;
  var regex_branch = /data-productskuid=\"(\d+)/g;
  var regex_detail = /product_sku : \['(\d+)/g;
  var regex_checkout = /product_sku : (.*),/g;

  var input = document.body.innerHTML;

  if (pageType == 'branch') {
    regex = regex_branch;
  } else if (pageType == 'detail') {
    regex = regex_detail;
  } else if (pageType == 'checkout') {
    regex = regex_checkout
  }


  var product_sku_list = rgx_data(input, regex);
  // console.log(product_sku_list);


  //BEGIN CHECKOUT PAGE CODE
  // console.log(output[0]);
  // console.log(JSON.parse(output[0].replace(/'/g, '"')));
  // END CHECKOUT PAGE CODE


  // CREATE PRODUCT ID STRING FOR AJAX CALL
  var skuid_string = '';

  for (var i = 0; i < product_sku_list.length;i++) {
    if (i == product_sku_list.length - 1) {
      skuid_string += product_sku_list[i];
    } else {
      skuid_string += product_sku_list[i]+",";
    }
  }

  $.ajax({
       url: 'https://eproductmw.colruytgroup.com/eproductmw/rest/v1/nl/stores/3515/products.json?id='+skuid_string,
       headers: { 'Authorization': AUTH_TOKEN},
       type: 'GET',
       success: responseHandler
     });


  function responseHandler(res) {
    console.log(res);

    for (var i = 0; i < res.data.list.length; i++) {
      var data = res.data.list[i];

      product_sku = data.commercialId;

      if (pageType == 'branch') {
        var tar_element = $("div[data-productskuid='"+product_sku+"']"); //issues with meat > different product_sku and commercial id from json.
      } else if (pageType == 'detail') {
        var tar_element = $( ".product__body" );
      }

      if (data.price1) { //if we have no price, we can't compare.
        col_price1 = data.price1;

        cog_price1_el = tar_element.find(".product__price-piece");

        if (pageType == 'branch') {
          cog_price1 = cog_price1_el.find(".displayPrice1").text();
        } else if (pageType == 'detail') {
          cog_price1 = cog_price1_el.find("#price1").text();
        }

        percentage = Math.round(100-((Number(col_price1.replace(',', '.'))/Number(cog_price1.replace(',', '.')))*100));

        var percentage_text='';

        if (Math.sign(percentage) == 1) {
          percentage_text = percentage+"&#37; duurder";
        } else {
          percentage_text = Math.abs(percentage)+"&#37; goedkoper";
        }

        var col_price1_el = document.createElement('div');
        col_price1_el.setAttribute("class", "extra");

        if (percentage != 0) {
          col_price1_el.innerHTML = "€&nbsp;" + col_price1 + "<span class='product__unit'>/"+data.price1Unit+"</span>&nbsp;&nbsp;&nbsp;&nbsp;"+percentage_text;
        } else {
          col_price1_el.innerHTML = "zelfde prijs!";
        }

        if (cog_price1 > col_price1) {
          cog_price1_el.addClass("red");
          // $(col_price1_el).addClass("green");
        } else if (cog_price1 < col_price1) {
          cog_price1_el.addClass("green");
          // $(col_price1_el).addClass("red");
        } else {
          $(col_price1_el).addClass("blue");
        }

        cog_price1_el.after($(col_price1_el));
      }
    }
  }

  }
