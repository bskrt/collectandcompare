# README #

This is a chrome extension that allows the user to compare prices from Colruyt collect and go with their store prices (from colruyt.be).
It's at a very alpha stage, but does work after some quick hacking (setting your authorization key).

### Next steps ###

-  ~~Get authorization token straight from your XTRA (colruyt.be) login session.~~
- Enable/disable by clicking the extension button.
- Checkout page with total price difference.
- Save the preferred store for better comparison.

### ISSUES ###

- Meats (beenhouwerij) have different product_sku (uid) than the one being returned from the json file, though the product_sku does call the correct product (interlinked in backend). The issue at hand is that we can't use the id returned from the json file to target the correct DOM element. Possible solution is to use something else as a reference eg. product title. Another solution would be to generate a dictionary with key:value pairs, linking the json id to the product_sku.
